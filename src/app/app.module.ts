import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';

// Modules
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './components/core/core.module';
import { AppStoreModule } from './store/app-store.module';
import { NgxBootstrapModule } from './shared/modules/ngx-bootstrap.module';

// Components
import { AppComponent } from './app.component';

// Services
import { StorageService } from './shared/services/storage.service';
import { HttpService } from './shared/services/http.service';

// Interceptors
import { ErrorInterceptor } from './shared/interceptor/error.interceptor';
import { TokenInterceptor } from './shared/interceptor/token.interceptor';
import { LoadingInterceptor } from './shared/interceptor/loading.interceptor';
@NgModule({
  declarations: [AppComponent],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    CoreModule,
    AppStoreModule,
    NgxBootstrapModule,
    NgxSpinnerModule
  ],
  providers: [
    HttpService,
    StorageService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
