import { UserActions, ActionsTypes } from './user.action';
import { UserVM } from 'src/app/shared/models/user.model';

export interface UserState {
    users: UserVM[];
    user: UserVM | any;
}

const initialState: UserState = {
    users: [],
    user: {}
};

export const userReducer: (state: UserState, action: UserActions) => UserState = (
    state: UserState = initialState,
    action: UserActions
) => {
    switch (action.type) {
        case ActionsTypes.LOAD_USERS: {
            return { ...state };
        }
        case ActionsTypes.LOAD_USERS_SUCCESS: {
            return { ...state, users: action.payload };
        }
        case ActionsTypes.LOAD_USER: {
            return { ...state };
        }
        case ActionsTypes.LOAD_USER_SUCCESS: {
            return { ...state, user: action.payload };
        }
        case ActionsTypes.UPDATE_USER: {
            return { ...state };
        }
        case ActionsTypes.UPDATE_USER_SUCCESS: {
            return { ...state, user: action.payload };
        }
        default:
            return state;
    }
};
