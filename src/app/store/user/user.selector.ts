import { createSelector } from '@ngrx/store';

import { UserState } from './../user/user.reducer';
import { AppState } from '..';

export const selectUserState = (state: AppState) => state.userReducer;

export const selectUsers = createSelector(selectUserState, (state: UserState) => state.users);

export const selectUser = createSelector(selectUserState, (state: UserState) => state.user);
