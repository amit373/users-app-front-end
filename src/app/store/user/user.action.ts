import { Action } from '@ngrx/store';

import { UserVM } from 'src/app/shared/models/user.model';

export enum ActionsTypes {
    LOAD_USERS = '[User] Load users',
    LOAD_USERS_SUCCESS = '[User] Load users success',
    LOAD_USER = '[User] Load user',
    LOAD_USER_SUCCESS = '[User] Load user success',
    UPDATE_USER = '[User] Update user',
    UPDATE_USER_SUCCESS = '[User] Update user success',
}

export class LoadUsers implements Action {
    readonly type = ActionsTypes.LOAD_USERS;
    constructor(
        public page?: number,
        public itemsPerPage?: number,
        public filter?: string,
        public search?: string
    ) { }
}

export class LoadUsersSuccess implements Action {
    readonly type = ActionsTypes.LOAD_USERS_SUCCESS;
    constructor(public payload: UserVM[]) { }
}

export class LoadUser implements Action {
    readonly type = ActionsTypes.LOAD_USER;
    constructor(public payload: string) { }
}

export class LoadUserSuccess implements Action {
    readonly type = ActionsTypes.LOAD_USER_SUCCESS;
    constructor(public payload: any) { }
}

export class UpdateUser implements Action {
    readonly type = ActionsTypes.UPDATE_USER;
    constructor(public id: string, public payload: any) { }
}


export class UpdateUserSuccess implements Action {
    readonly type = ActionsTypes.UPDATE_USER_SUCCESS;
    constructor(public payload: UserVM) { }
}


export type UserActions = LoadUsers | LoadUsersSuccess | LoadUser | LoadUserSuccess | UpdateUser | UpdateUserSuccess;
