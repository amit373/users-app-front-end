import { UserVM } from 'src/app/shared/models/user.model';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { tap, catchError, mergeMap, map } from 'rxjs/operators';
import * as _ from 'lodash';

import { UserService } from 'src/app/components/members/services/user.service';
import * as userActions from './user.action';
import * as asyncActions from '../async/async.action';
import { AppState } from './../index';
import { AlertifyService } from 'src/app/shared/services/alertify.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';

@Injectable()
export class UserEffects {
    constructor(
        private readonly action$: Actions,
        private readonly store: Store<AppState>,
        private readonly userService: UserService,
        private readonly alertify: AlertifyService,
        private readonly router: Router,
        private readonly authService: AuthService
    ) { }

    // Load All Users
    @Effect()
    loadUsers$: Observable<Action> = this.action$.pipe(
        ofType<userActions.LoadUsers>(userActions.ActionsTypes.LOAD_USERS),
        tap(() => this.store.dispatch(new asyncActions.AsyncActionStart())),
        mergeMap(({ page, itemsPerPage, filter, search }) =>
            this.userService.getUsers(page, itemsPerPage, filter, search).pipe(
                map((response: any) => {
                    const users: UserVM[] = response.data;
                    _.remove(users, ({ _id }) => _id === this.authService.getUserId);
                    this.store.dispatch(new asyncActions.AsyncActionFinish());
                    return new userActions.LoadUsersSuccess(users);
                }),
                catchError(({ error }) => of(new asyncActions.AsyncActionError(error)))
            )
        )
    );

    // Load User
    @Effect()
    loadUser$: Observable<Action> = this.action$.pipe(
        ofType<userActions.LoadUser>(userActions.ActionsTypes.LOAD_USER),
        tap(() => this.store.dispatch(new asyncActions.AsyncActionStart())),
        mergeMap(({ payload }) =>
            this.userService.getUser(payload).pipe(
                map((response: any) => {
                    const user: UserVM = response.data;
                    this.store.dispatch(new asyncActions.AsyncActionFinish());
                    return new userActions.LoadUserSuccess(user);
                }),
                catchError(({ error }) => {
                    this.alertify.error(error.message);
                    this.router.navigate(['/members']);
                    return of(new asyncActions.AsyncActionError(error));
                })
            )
        ));

    // Update User
    @Effect()
    updateUser$: Observable<Action> = this.action$.pipe(
        ofType<userActions.UpdateUser>(userActions.ActionsTypes.UPDATE_USER),
        tap(() => this.store.dispatch(new asyncActions.AsyncActionStart())),
        mergeMap(({ id, payload }) =>
            this.userService.updateUser(id, payload).pipe(
                map((response: any) => {
                    const user: UserVM = response.data;
                    this.store.dispatch(new asyncActions.AsyncActionFinish());
                    this.alertify.success('Profile Updated Successfully');
                    return new userActions.UpdateUserSuccess(user);
                }),
                catchError(({ error }) => {
                    this.alertify.error(error.message);
                    return of(new asyncActions.AsyncActionError(error));
                })
            )
        )
    );

}
