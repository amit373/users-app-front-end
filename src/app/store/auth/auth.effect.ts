import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { mergeMap, catchError, map, tap } from 'rxjs/operators';

import { AppState } from '..';
import { LoginRsp } from 'src/app/shared/models/auth.model';
import * as authActions from './auth.action';
import * as asyncActions from '../async/async.action';

import { AlertifyService } from 'src/app/shared/services/alertify.service';
import { StorageService } from '../../shared/services/storage.service';
import { AuthService } from '../../shared/services/auth.service';

@Injectable()
export class AuthEffects {
  constructor(
    private readonly effectActions$: Actions,
    private readonly store: Store<AppState>,
    private readonly storageService: StorageService,
    private readonly router: Router,
    private readonly alertifyService: AlertifyService,
    private authService: AuthService,
  ) { }

  // Login Effect
  @Effect()
  loginUser$: Observable<Action> = this.effectActions$.pipe(
    ofType<authActions.LoginUser>(authActions.ActionsTypes.LOGIN_USER),
    tap(() => this.store.dispatch(new asyncActions.AsyncActionStart())),
    mergeMap((action: authActions.LoginUser) =>
      this.authService.login(action.payload).pipe(
        map((user: LoginRsp) => {
          delete user.message;
          delete user.success;
          this.storageService.saveToLocalStorage('currentUser', user);
          this.authService.currentUserSubject.next(user);
          this.store.dispatch(new asyncActions.AsyncActionFinish());
          this.alertifyService.success('Login Successfully!');
          return new authActions.SetCurrentUser(user);
        }),
        catchError(err => {
          this.store.dispatch(new authActions.ResetState());
          return of(new asyncActions.AsyncActionError(err.error));
        })
      )
    )
  );

  // Register Effect
  @Effect()
  registerUser$: Observable<Action> = this.effectActions$.pipe(
    ofType<authActions.RegisterUser>(authActions.ActionsTypes.REGISTER_USER),
    tap(() => this.store.dispatch(new asyncActions.AsyncActionStart())),
    mergeMap((action: authActions.RegisterUser) =>
      this.authService.register(action.payload).pipe(
        map((user: LoginRsp) => {
          delete user.message;
          delete user.success;
          this.storageService.saveToLocalStorage('currentUser', user);
          this.authService.currentUserSubject.next(user);
          this.store.dispatch(new asyncActions.AsyncActionFinish());
          this.alertifyService.success('Register Successfully!');
          return new authActions.SetCurrentUser(user);
        }),
        catchError(err => {
          this.store.dispatch(new authActions.ResetState());
          return of(new asyncActions.AsyncActionError(err.error));
        })
      )
    )
  );

  // Set Initial User
  @Effect({ dispatch: false })
  SetInitialUser$: Observable<any> = this.effectActions$.pipe(
    ofType(authActions.ActionsTypes.SET_INITIAL_USER),
    tap(() => this.store.dispatch(new asyncActions.AsyncActionStart())),
    map(() => {
      const currentUser: LoginRsp = this.storageService.getFromLocalStorage('currentUser');
      this.authService.currentUserSubject.next(currentUser);
      this.store.dispatch(new authActions.SetCurrentUser(currentUser));
      this.store.dispatch(new asyncActions.AsyncActionFinish());
    })
  );

  // ResetState
  @Effect({ dispatch: false })
  ResetState$: Observable<any> = this.effectActions$.pipe(
    ofType(authActions.ActionsTypes.RESET_STATE),
    map(() => {
      this.storageService.resetStorage();
      this.authService.currentUserSubject.next(null);
      this.router.navigate(['/']);
      this.alertifyService.message('Logout Successfully!');
    })
  );

}
