import { createSelector } from '@ngrx/store';

import { AuthState } from './auth.reducer';
import { AppState } from '..';

export const selectAuthState = (state: AppState) => state.authReducer;

export const selectCurrentUser = createSelector(selectAuthState, (state: AuthState) => state.user);

export const selectIsAuthenticated = createSelector(selectAuthState, (state: AuthState) => state.isAuthenticated);
