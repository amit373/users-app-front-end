import { ActionsTypes, AuthActions } from './auth.action';
import { LoginRsp } from '../../shared/models/auth.model';

export interface AuthState {
  user: LoginRsp | any;
  isAuthenticated: boolean;
}

const initialState: AuthState = {
  user: null,
  isAuthenticated: false
};

export const authReducer: (state: AuthState, action: AuthActions) => AuthState = (
  state = initialState,
  action: AuthActions
) => {
  switch (action.type) {
    case ActionsTypes.SET_INITIAL_USER: {
      return { ...state };
    }
    case ActionsTypes.LOGIN_USER:
      return { ...state };
    case ActionsTypes.REGISTER_USER: {
      return { ...state };
    }
    case ActionsTypes.SET_CURRENT_USER: {
      const { user, token } = action.payload;
      return {
        ...state,
        isAuthenticated: true,
        user: { ...user, token },
      };
    }
    case ActionsTypes.RESET_STATE: {
      return {
        ...state,
        isAuthenticated: false,
        user: null,
      };
    }
    default:
      return state;
  }
};
