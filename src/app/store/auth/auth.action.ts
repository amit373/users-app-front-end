import { Action } from '@ngrx/store';
import { AuthVM, LoginRsp } from 'src/app/shared/models/auth.model';

export enum ActionsTypes {
  LOGIN_USER = '[AUTH] Login user',
  SET_CURRENT_USER = '[AUTH] Set Current user',
  SET_INITIAL_USER = '[AUTH] Set initial user',
  REGISTER_USER = '[AUTH] Register user',
  RESET_STATE = '[Auth] Reset State'
}

export class SetInitialUser implements Action {
  readonly type = ActionsTypes.SET_INITIAL_USER;
}

export class LoginUser implements Action {
  readonly type = ActionsTypes.LOGIN_USER;
  constructor(public payload: AuthVM) { }
}

export class SetCurrentUser implements Action {
  readonly type = ActionsTypes.SET_CURRENT_USER;
  constructor(public payload: LoginRsp) { }
}

export class RegisterUser implements Action {
  readonly type = ActionsTypes.REGISTER_USER;
  constructor(public payload: AuthVM) { }
}

export class ResetState implements Action {
  readonly type = ActionsTypes.RESET_STATE;
  constructor() { }
}

export type AuthActions = LoginUser | SetCurrentUser | SetInitialUser | RegisterUser | ResetState;
