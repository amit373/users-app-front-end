import { ActionReducerMap, MetaReducer, ActionReducer } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
import { storeLogger } from 'ngrx-store-logger';

import { environment } from 'src/environments/environment';
// Effects
import { AuthEffects } from './auth/auth.effect';
import { UserEffects } from './user/user.effects';
// Reducers
import { authReducer, AuthState } from './auth/auth.reducer';
import { RouterStateUrl } from './router/router.reducer';
import { userReducer, UserState } from './user/user.reducer';
import { asyncReducer } from './async/async.reducer';

export const effects = [AuthEffects, UserEffects];

export const reducers: ActionReducerMap<AppState> = {
  authReducer,
  router: fromRouter.routerReducer,
  asyncReducer,
  userReducer
};

export interface AppState {
  authReducer: AuthState;
  router: fromRouter.RouterReducerState<RouterStateUrl>;
  asyncReducer;
  userReducer: UserState;
}

export const selectAppState = (state: AppState): AppState => state;

export function logger(reducer: ActionReducer<AppState>): any {
  return storeLogger()(reducer);
}

export const metaReducers: Array<MetaReducer<AppState>> = !environment.production ? [storeFreeze,
  // logger
] : [];
