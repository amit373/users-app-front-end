import { AsyncActions, ActionsTypes } from './async.action';

export interface AsyncState {
    loading: boolean;
    error: any;
}

const initialState: AsyncState = {
    loading: false,
    error: null
};

export const asyncReducer: (state: AsyncState, action: AsyncActions) =>
    AsyncState = (state = initialState, action: AsyncActions) => {
        switch (action.type) {
            case ActionsTypes.ASYNC_ACTION_START:
                return {
                    ...state,
                    loading: true,
                    error: null
                };
            case ActionsTypes.ASYNC_ACTION_FINISH:
                return {
                    ...state,
                    loading: false,
                    error: null
                };
            case ActionsTypes.ASYNC_ACTION_ERROR:
                return {
                    ...state,
                    loading: false,
                    error: action.payload
                };
            default:
                return state;
        }
    };
