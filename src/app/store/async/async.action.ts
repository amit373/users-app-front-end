import { Action } from '@ngrx/store';

export enum ActionsTypes {
    ASYNC_ACTION_START = '[Async] Action Start',
    ASYNC_ACTION_FINISH = '[Async] Action Finish',
    ASYNC_ACTION_ERROR = '[Async] Action Error'
}

export class AsyncActionStart implements Action {
    readonly type = ActionsTypes.ASYNC_ACTION_START;
}

export class AsyncActionFinish implements Action {
    readonly type = ActionsTypes.ASYNC_ACTION_FINISH;
}

export class AsyncActionError implements Action {
    readonly type = ActionsTypes.ASYNC_ACTION_ERROR;
    constructor(public payload: any) { }
}

export type AsyncActions = AsyncActionStart | AsyncActionFinish | AsyncActionError;
