import { createSelector } from '@ngrx/store';

import { AsyncState } from './async.reducer';
import { AppState } from '..';

export const selectAsyncState = (state: AppState) => state.asyncReducer;

export const selectLoading = createSelector(selectAsyncState, (state: AsyncState) => state.loading);

export const selectError = createSelector(selectAsyncState, (state: AsyncState) => state.error);
