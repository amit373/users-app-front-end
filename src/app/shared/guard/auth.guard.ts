import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  UrlSegment,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppState } from '../../store';
import { selectIsAuthenticated } from 'src/app/store/auth/auth.selector';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private readonly store: Store<AppState>) { }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean> | Promise<boolean> | boolean {
    return this.checkIsAuthenticated();
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean> | Promise<boolean> | boolean {
    return this.checkIsAuthenticated();
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkIsAuthenticated();
  }

  private async checkIsAuthenticated(): Promise<boolean> {
    let isAuth: boolean = false;
    await this.store.select(selectIsAuthenticated).subscribe((isAuthenticated: boolean) => {
      isAuth = isAuthenticated ? true : false;
    });
    if (isAuth) {
      return true;
    } else {
      return false;
    }
  }
}
