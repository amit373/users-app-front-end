import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'age' })
export class AgePipe implements PipeTransform {

    transform(date: string, formate: string = 'years'): number | string {
        if (!date || typeof date !== 'string') { return ''; }
        if (formate === 'years') {
            return moment().diff(date, 'years');
        } else if (formate === 'days') {
            return moment().diff(date, 'days');
        }
    }
}
