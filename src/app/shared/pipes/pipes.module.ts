import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimeElapsedPipe } from './time-elapsed.pipe';
import { AgePipe } from './age.pipe';

const pipes = [
    TimeElapsedPipe,
    AgePipe
];

@NgModule({
    declarations: [...pipes],
    exports: [...pipes],
    imports: [CommonModule]
})
export class PipesModule { }
