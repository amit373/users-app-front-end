import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

enum snipperColors {
    bgColor = 'rgba(255,255,255,0.7)',
    color = '#333333'
}

@Injectable({ providedIn: 'root' })
export class SpinnerService {
    private busyRequestCount: number = 0;
    constructor(private readonly spinnerService: NgxSpinnerService) { }

    public busy(): void {
        this.busyRequestCount++;
        this.spinnerService.show(undefined, {
            type: 'timer',
            bdColor: snipperColors.bgColor,
            color: snipperColors.color
        });
    }

    public idle(): void {
        this.busyRequestCount--;
        if (this.busyRequestCount <= 0) {
            this.busyRequestCount = 0;
            this.spinnerService.hide();
        }
    }
}
