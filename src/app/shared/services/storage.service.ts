import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StorageService {

  public saveToLocalStorage = (key: any, value: any): void => {
    localStorage[key] = JSON.stringify(value);
  }

  public getFromLocalStorage = (key: string): any => {
    if (localStorage[key]) {
      return JSON.parse(localStorage[key]);
    } else {
      return false;
    }
  }

  public removeFromLocalStorage = (key: string): any => {
    localStorage.removeItem(key);
  }

  public saveToSession = (key: any, value: any): void => {
    sessionStorage[key] = JSON.stringify(value);
  }

  public getSessionStorage = (key: string): any => {
    if (sessionStorage[key]) {
      return JSON.parse(sessionStorage[key]);
    } else {
      return false;
    }
  }

  public removeFromSessionStorage = (key: string): void => {
    sessionStorage.removeItem(key);
  }

  public resetStorage(): void {
    localStorage.clear();
    sessionStorage.clear();
  }
}
