import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from './../../../environments/environment';
@Injectable({ providedIn: 'root' })
export class HttpService {
  constructor(public readonly http: HttpClient) { }

  public get(endpoint: string): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}/${endpoint}`);
  }

  public post(endpoint: string, data: any): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/${endpoint}`, data);
  }

  public put(endpoint: string, data: any): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}/${endpoint}`, data);
  }

  public delete(endpoint: string, id: number | string): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}/${endpoint}/${id}`);
  }

  public patch(endpoint: string, id: number | string, data: any): Observable<any> {
    return this.http.patch<any>(`${environment.apiUrl}/${endpoint}/${id}`, data);
  }
}
