import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({ providedIn: 'root' })
export class DateService {

    public getRangeOfDates(startAt: string, endAt: string): string[] {
        const dates: Array<any> = [];
        let mStartAt = moment(new Date(startAt));
        const mEndAt = moment(new Date(endAt));
        while (mStartAt < mEndAt) {
            dates.push(mStartAt.format());
            mStartAt = mStartAt.add(1, 'day');
        }
        return dates;
    }

    public isDateInPast(date: moment.Moment): boolean {
        return date.diff(moment(), 'days') < 0;
    }

    public format(date: string, dateFormat = 'YYYY/MM/DD'): string {
        if (!date) { return ''; }
        return moment(date).format(dateFormat);
    }

    public calendar(date: string): string {
        if (!date) { return ''; }
        return moment(date).calendar(date, {
            sameDay: '[Today]',
            nextDay: '[Tomorrow]',
            lastDay: '[Yesterday]',
            nextWeek: 'dddd MMM DD',
            lastWeek: '[Last] dddd',
            sameElse: 'DD/MM/YYYY'
        });
    }

    public fromNow(date: string): string {
        if (!date) { return ''; }
        return moment(date).fromNow();
    }
}
