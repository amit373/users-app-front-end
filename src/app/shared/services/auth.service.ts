import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
import { Store } from '@ngrx/store';
import { JwtHelperService } from '@auth0/angular-jwt';

import { HttpService } from './http.service';
import { StorageService } from './storage.service';
import { AuthVM, LoginRsp } from '../models/auth.model';
import { AppState } from '../../store';
import { ResetState } from 'src/app/store/auth/auth.action';

@Injectable()
export class AuthService {
    public currentUserSubject: BehaviorSubject<LoginRsp>;
    public currentUser: Observable<LoginRsp>;
    public defaultImg: string = '/assets/images/user.png';
    public JwtHelperService = new JwtHelperService();
    constructor(
        private readonly storageService: StorageService,
        private readonly httpService: HttpService,
        private readonly store: Store<AppState>
    ) {
        this.currentUserSubject = new BehaviorSubject<LoginRsp>(
            this.storageService.getFromLocalStorage('currentUser')
        );
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }

    public setToken(token: string): string | any {
        this.storageService.saveToLocalStorage('authToken', token);
    }

    public getToken(): string {
        const { token } = this.storageService.getFromLocalStorage('currentUser');
        return token;
    }

    public checkAuthentication(): boolean {
        const { token } = this.storageService.getFromLocalStorage('currentUser');
        if (!token) { return false; }
        return !this.JwtHelperService.isTokenExpired(token);
    }

    get getCurrentUser(): string {
        return this.storageService.getFromLocalStorage('currentUser') || '';
    }

    get isAuthenticated(): boolean {
        return moment().isBefore(this.expiration);
    }

    private get expiration() {
        const { token } = this.storageService.getFromLocalStorage('currentUser');
        if (token) {
            const { exp } = this.JwtHelperService.decodeToken(token);
            return moment.unix(exp);
        }
    }

    public get getUserId(): any {
        const { token } = this.storageService.getFromLocalStorage('currentUser');
        if (token) {
            const { id } = this.JwtHelperService.decodeToken(token);
            return id;
        }
    }

    public login(data: AuthVM): Observable<any> {
        return this.httpService.post('auth/login', data);
    }

    public register(data: AuthVM): Observable<any> {
        return this.httpService.post('auth/register', data);
    }

    public logout(): void {
        this.store.dispatch(new ResetState());
    }

}
