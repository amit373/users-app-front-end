import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { AuthService } from '../services/auth.service';

@Injectable({ providedIn: 'root' })
export class TokenInterceptor implements HttpInterceptor {
  constructor(public readonly authService: AuthService) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken: string = this.authService.getToken();
    const isApiUrl: boolean = request.url.startsWith(environment.apiUrl);
    if (authToken && isApiUrl) {
      request = request.clone({ setHeaders: { Authorization: `Bearer ${authToken}` } });
    }
    return next.handle(request);
  }
}
