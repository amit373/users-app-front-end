export class UserVM {
    _id: string;
    username: string;
    password: string;
    knownAs: string;
    dateOfBirth: string;
    gender: string;
    lastActive: string;
    city: string;
    country: string;
    photoUrl: string;
    createdAt: string;
    interests?: string;
    lookingFor?: string;
    photos?: PhotoVM[];
    introduction?: string;
}

export interface PhotoVM {
    id: string;
    url: string;
    descrption: string;
    dateAdded: Date;
    isMain: boolean;
}
