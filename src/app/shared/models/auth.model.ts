export class AuthVM {
    username: string;
    password: string;
}

export class LoginRsp {
    success?: boolean;
    message?: string;
    user: { _id: string; username: string };
    token: string;
    iat?: number;
    exp?: number;
}
