export interface PaginationVM {
    currentPage: number;
    itemsPerPage: number;
}

export class PaginationResult<T> {
    result: T;
    pagination: PaginationVM;
}