import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';

import { AppState } from './../../../store';
import { LoadUser } from '../../../store/user/user.action';
import { UserVM } from 'src/app/shared/models/user.model';

@Injectable({ providedIn: 'root' })
export class MemberDetailResolver implements Resolve<UserVM> {
    constructor(private readonly store: Store<AppState>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
        : Observable<UserVM> | Promise<UserVM> | any {
        return this.store.select(({ router: { state: { params } } }) => params.id)
            .pipe(take(1), map(id => this.store.dispatch(new LoadUser(id))));
    }
}
