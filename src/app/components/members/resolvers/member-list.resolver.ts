import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppState } from './../../../store';
import { LoadUsers } from 'src/app/store/user/user.action';
import { UserVM } from 'src/app/shared/models/user.model';

@Injectable({ providedIn: 'root' })
export class MemberListResolver implements Resolve<UserVM[]> {
    constructor(private readonly store: Store<AppState>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
        : Observable<UserVM[]> | Promise<UserVM[]> | any {
        return this.store.dispatch(new LoadUsers());
    }
}
