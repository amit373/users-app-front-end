import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppState } from './../../../store';
import { LoadUser } from 'src/app/store/user/user.action';
import { UserVM } from 'src/app/shared/models/user.model';
import { AuthService } from 'src/app/shared/services/auth.service';

@Injectable({ providedIn: 'root' })
export class MemberEditResolver implements Resolve<UserVM> {
    constructor(
        private readonly store: Store<AppState>,
        private readonly authService: AuthService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
        : Observable<UserVM> | Promise<UserVM> | any {
        return this.store.dispatch(new LoadUser(this.authService.getUserId));
    }
}