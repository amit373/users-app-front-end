import { Injectable } from '@angular/core';
import { MemberEditComponent } from '../components/member-edit/member-edit.component';
import { CanDeactivate, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class PreventUnsavedChanges implements CanDeactivate<MemberEditComponent> {
    constructor() { }
    canDeactivate(component: MemberEditComponent):
        Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (component.editForm.dirty) {
            return confirm('Are you sure want to continue ? Any unsaved changes will be lost');
        }
        return true;
    }

}
