import { Component, OnInit, Input } from '@angular/core';

import { UserVM } from '../../../../shared/models/user.model';
import { AuthService } from '../../../../shared/services/auth.service';
@Component({
  selector: 'app-member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.scss']
})
export class MemberCardComponent implements OnInit {
  @Input() user: UserVM;
  constructor(public readonly authService: AuthService) { }
  ngOnInit(): void { }
}
