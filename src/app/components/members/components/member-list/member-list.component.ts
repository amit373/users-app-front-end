import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '../../../../store';
import { selectUsers } from '../../../../store/user/user.selector';
import { UserVM } from '../../../../shared/models/user.model';
import { LoadUsers } from 'src/app/store/user/user.action';
@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.scss']
})
export class MemberListComponent implements OnInit {
  users: Observable<UserVM[]>;
  currentPage: number = 1;
  totalItems: number = 10;
  itemsPerPage: number = 6;
  genderList = [{ value: 'male', display: 'Males' }, { value: 'female', display: 'Females' }];
  userParams: any = {};
  constructor(private readonly store: Store<AppState>) { }

  async ngOnInit(): Promise<void> {
    this.users = await this.store.select(selectUsers);
  }

  public pageChanged({ page, itemsPerPage }: any): void {
    this.currentPage = page;
    this.itemsPerPage = itemsPerPage;
    this.store.dispatch(new LoadUsers(page, itemsPerPage));
  }

  public onSearch(filter: string): void {
    if (filter) {
      this.store.dispatch(new LoadUsers(this.currentPage, this.itemsPerPage, filter));
    }
  }

  public loadUsers(): void {
    const { gender, search } = this.userParams;
    this.store.dispatch(new LoadUsers(this.currentPage, this.itemsPerPage, search, gender));
  }

  public resetFilters(): void {
    this.userParams.gender = this.userParams.gender === '';
    this.userParams.minAge = 18;
    this.userParams.maxAge = 99;
    this.userParams.search = '';
    this.store.dispatch(new LoadUsers());
  }
}

