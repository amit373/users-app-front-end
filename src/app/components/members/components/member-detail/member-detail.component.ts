import { PhotoVM } from './../../../../shared/models/user.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery-9';
import * as _ from 'lodash';

import { AppState } from '../../../../store';
import { selectUser } from '../../../../store/user/user.selector';
import { AuthService } from '../../../../shared/services/auth.service';
import { UserVM } from 'src/app/shared/models/user.model';
@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.scss']
})
export class MemberDetailComponent implements OnInit, OnDestroy {
  user: UserVM;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[] = [];
  userSub$: Subscription;
  constructor(
    public readonly authService: AuthService,
    private readonly store: Store<AppState>
  ) { }

  async ngOnInit(): Promise<void> {
    this.userSub$ = await this.store.select(selectUser).subscribe((user: UserVM) => {
      this.user = user;
      this.galleryOptionsHandler();
      this.galleryImages = this.getImages();
    });
  }

  private getImages() {
    if (this.user.photos && this.user.photos.length > 0) {
      const imageUrls: any[] = [];
      _.forEach(this.user.photos, (photo: PhotoVM) => {
        imageUrls.push({
          small: photo.url,
          medium: photo.url,
          big: photo.url,
          description: photo.descrption,
        });
      });
      return imageUrls;
    }
  }

  private galleryOptionsHandler(): void {
    this.galleryOptions = [
      { width: '600px', height: '400px', thumbnailsColumns: 4, imageAnimation: NgxGalleryAnimation.Slide },
      {
        breakpoint: 800, width: '100%', height: '00px', imagePercent: 100, thumbnailsColumns: 4,
        thumbnailsPercent: 20, thumbnailsMargin: 20, thumbnailMargin: 20
      },
      { breakpoint: 400, preview: false }
    ];
  }

  public ngOnDestroy(): void {
    if (this.userSub$) {
      this.userSub$.unsubscribe();
    }
  }
}
