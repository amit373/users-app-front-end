import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { AuthService } from '../../../../shared/services/auth.service';
import { UserVM } from 'src/app/shared/models/user.model';
import { AppState } from '../../../../store';
import { selectUser } from 'src/app/store/user/user.selector';
import { selectLoading } from '../../../../store/async/async.selector';
import { UpdateUser } from 'src/app/store/user/user.action';
@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.scss']
})
export class MemberEditComponent implements OnInit, OnDestroy {
  user: UserVM;
  userSub$: Subscription;
  editForm: FormGroup;
  isLoading$: Observable<boolean>;

  // For Native Browser Tab Close
  @HostListener('window:beforeunload', ['$event'])
  uploadNotification($event: any) {
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }

  constructor(
    public readonly authService: AuthService,
    private readonly store: Store<AppState>,
    private readonly fb: FormBuilder
  ) {
    this.user = new UserVM();
    this.initForm();
  }

  async ngOnInit(): Promise<void> {
    await this.setFormValues();
  }

  private async setFormValues(): Promise<void> {
    this.isLoading$ = await this.store.select(selectLoading);
    this.userSub$ = await this.store.select(selectUser).subscribe((user: UserVM) => {
      this.user = user;
      this.editForm.patchValue(user);
    });
  }

  protected initForm(): void {
    this.editForm = this.fb.group({
      city: [''],
      country: [''],
      interests: [''],
      lookingFor: [''],
      introduction: [''],
    });
  }

  async updateUser(): Promise<void> {
    if (!this.editForm.invalid) {
      const userId: string = this.user._id;
      const { city, country, interests, lookingFor, introduction } = this.editForm.value;
      const updatedUserObj: Partial<UserVM> = { city, country, interests, lookingFor, introduction };
      await this.store.dispatch(new UpdateUser(userId, updatedUserObj));
      await this.editForm.reset();
      await this.setFormValues();
    }
  }

  updateMainPhoto($event) { }

  public ngOnDestroy(): void {
    if (this.userSub$) {
      this.userSub$.unsubscribe();
    }
  }
}
