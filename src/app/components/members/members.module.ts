import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxGalleryModule } from 'ngx-gallery-9';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';

// Modules
import { MembersRoutingModule } from './members-routing.module';
import { NgxBootstrapModule } from 'src/app/shared/modules/ngx-bootstrap.module';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';

// Components
import { MembersComponent } from './members.component';
import { MemberListComponent } from './components/member-list/member-list.component';
import { MemberCardComponent } from './components/member-card/member-card.component';
import { MemberDetailComponent } from './components/member-detail/member-detail.component';
import { MemberEditComponent } from './components/member-edit/member-edit.component';
import { PhotoEditorComponent } from './components/photo-editor/photo-editor.component';

// Guard
import { PreventUnsavedChanges } from './guard/prevent-unsaved-changes.guard';
@NgModule({
  declarations: [
    MembersComponent,
    MemberListComponent,
    MemberCardComponent,
    MemberDetailComponent,
    MemberEditComponent,
    PhotoEditorComponent
  ],
  imports: [
    CommonModule,
    MembersRoutingModule,
    NgxBootstrapModule,
    NgxGalleryModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    FileUploadModule
  ],
  providers: [PreventUnsavedChanges]
})
export class MembersModule { }
