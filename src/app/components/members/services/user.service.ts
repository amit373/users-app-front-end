import { PaginationResult } from './../../../shared/models/pagination.model';
import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared/services/http.service';
import { Observable } from 'rxjs';

import { UserVM } from '../../../shared/models/user.model';
import { HttpParams } from '@angular/common/http';
@Injectable({ providedIn: 'root' })

export class UserService {
    constructor(private readonly httpService: HttpService) { }

    public getUsers(page?, itemsPerPage?, filter?, gender?): Observable<UserVM[]> {
        let url: string = 'auth/users?page=1&limit=6';
        // Pagination
        if (page && itemsPerPage) {
            url = `auth/users?page=${page}&limit=${itemsPerPage}`;
        }
        // Search
        if (page && itemsPerPage && filter) {
            url = `auth/users?page=${page}&limit=${itemsPerPage}&knownAs=${filter}`;
        }
        // Gender
        if (page && itemsPerPage && gender) {
            url = `auth/users?page=${page}&limit=${itemsPerPage}&gender=${gender}`;
        }
        // All Combine
        if (page && itemsPerPage && filter && gender) {
            url = `auth/users?page=${page}&limit=${itemsPerPage}&knownAs=${filter}&gender=${gender}`;
        }
        return this.httpService.get(url);
    }

    public getUser(id: string): Observable<UserVM> {
        return this.httpService.get(`auth/users/${id}`);
    }

    public updateUser(id: string, user): Observable<any> {
        return this.httpService.put(`auth/users/${id}`, user);
    }

    public deleteUser(id: string): Observable<any> {
        return this.httpService.delete(`auth/users/`, id);
    }

    public setMainPhoto(userId: string, photoId: string | number): Observable<any> {
        return this.httpService.post(`auth/users/${userId}/photo/${photoId}/setMain`, {});
    }

    public deletePhoto(userId: string, id: number | string): Observable<any> {
        return this.httpService.delete(`auth/users/${userId}/photos`, id);
    }

}

