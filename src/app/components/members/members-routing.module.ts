import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { MembersComponent } from './members.component';
import { MemberDetailComponent } from './components/member-detail/member-detail.component';
import { MemberEditComponent } from './components/member-edit/member-edit.component';

// Resolvers
import { MemberListResolver } from './resolvers/member-list.resolver';
import { MemberDetailResolver } from './resolvers/member-detail.resolver';
import { MemberEditResolver } from './resolvers/member-edit.resolver';

// Guard
import { PreventUnsavedChanges } from './guard/prevent-unsaved-changes.guard';

const routes: Routes = [
  {
    path: '', component: MembersComponent,
    resolve: { data: MemberListResolver },
  },
  {
    path: 'edit',
    component: MemberEditComponent,
    resolve: { data: MemberEditResolver },
    canDeactivate: [PreventUnsavedChanges]
  },
  {
    path: ':id',
    component: MemberDetailComponent,
    resolve: { data: MemberDetailResolver }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MembersRoutingModule { }
