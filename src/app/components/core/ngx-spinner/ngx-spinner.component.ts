import { Component } from '@angular/core';

@Component({
  selector: 'app-ngx-spinner',
  template: `
    <ngx-spinner
      bdOpacity="0.9"
      bdColor="rgba(51,51,51,0.47)"
      size="default"
      color="#fff"
      type="ball-spin-clockwise"
      [fullScreen]="true"
    >
      <p style="color: white;">Loading...</p>
    </ngx-spinner>`
})
export class NgxSpinnerComponent { }
