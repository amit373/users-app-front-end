import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription, Observable } from 'rxjs';
import { BsDropdownConfig } from 'ngx-bootstrap/dropdown';

import { AuthService } from 'src/app/shared/services/auth.service';
import { AppState } from '../../../store';
import { LoginUser } from 'src/app/store/auth/auth.action';
import { selectCurrentUser, selectIsAuthenticated, selectAuthState } from 'src/app/store/auth/auth.selector';
import { selectLoading } from 'src/app/store/async/async.selector';
import { UserVM } from 'src/app/shared/models/user.model';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [{ provide: BsDropdownConfig, useValue: { isAnimated: true, autoClose: true, } }]
})
export class NavbarComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  submitted: boolean = false;
  subscription$: Subscription;
  isAuthenticated: Observable<boolean>;
  currentUser: Observable<Partial<UserVM>>;
  isLoading: Observable<boolean>;
  constructor(
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly store: Store<AppState>,
    public readonly authService: AuthService
  ) { }

  async ngOnInit(): Promise<void> {
    this.formInit();
    this.isLoading = await this.store.select(selectLoading);
    this.isAuthenticated = await this.store.select(selectIsAuthenticated);
    this.currentUser = await this.store.select(selectCurrentUser);
  }

  private formInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  get f() { return this.loginForm.controls; }

  public async onSubmit(): Promise<void> {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    await this.store.dispatch(new LoginUser(this.loginForm.value));
    this.subscription$ = await this.store.select(selectAuthState).subscribe(({ user }) => {
      if (user && user.token) {
        this.router.navigate(['/']);
      }
    });
  }

  public onLogout(): void {
    this.authService.logout();
  }

  public ngOnDestroy(): void {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }
}


