import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription, Observable } from 'rxjs';

import { AppState } from '../../../store';
import { RegisterUser } from 'src/app/store/auth/auth.action';
import { selectAsyncState } from '../../../store/async/async.selector';
import { selectLoading, selectError } from 'src/app/store/async/async.selector';
import { MustMatch } from 'src/app/shared/helpers/must-match.validator';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  @Output() cancelRegister = new EventEmitter<boolean>();
  registerForm: FormGroup;
  submitted: boolean = false;
  subscription$: Subscription;
  isLoading: Observable<boolean>;
  errors: Observable<any>;
  bsConfig: Partial<BsDatepickerConfig>;
  constructor(
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.bsConfig = {
      containerClass: 'theme-red'
    };
    this.isLoading = this.store.select(selectLoading);
    this.errors = this.store.select(selectError);
    this.formInit();
  }

  formInit(): void {
    this.registerForm = this.fb.group({
      gender: ['male'],
      username: ['', Validators.required],
      knownAs: ['', Validators.required],
      dateOfBirth: [null, Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(8)]],
      confirmPassword: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  get f() { return this.registerForm.controls; }

  async onSubmit(): Promise<any> {
    this.submitted = true;
    const { invalid, value } = this.registerForm;
    if (invalid) {
      return;
    }
    delete value.confirmPassword;
    await this.store.dispatch(new RegisterUser(value));
    await this.store
      .select(state => state.authReducer)
      .subscribe(({ user }) => {
        if (user && user.token) {
          this.registerForm.reset();
          this.submitted = false;
          this.cancelRegister.emit(false);
          this.router.navigate(['/']);
        }
      });
  }

  cancel(): void {
    this.cancelRegister.emit(false);
  }

  ngOnDestroy(): void {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

}
