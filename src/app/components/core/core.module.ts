import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';

// Modules
import { NgxBootstrapModule } from 'src/app/shared/modules/ngx-bootstrap.module';

// Components
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from 'src/app/components/core/navbar/navbar.component';
import { NgxSpinnerComponent } from './ngx-spinner/ngx-spinner.component';
import { NotFoundComponent } from 'src/app/components/core/not-found/not-found.component';
import { RegisterComponent } from './register/register.component';
import { ServerErrorComponent } from './server-error/server-error.component';

// Services
import { AuthService } from '../../shared/services/auth.service';

const components = [
  NavbarComponent,
  HomeComponent,
  RegisterComponent,
  NotFoundComponent,
  ServerErrorComponent,
  NgxSpinnerComponent
];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    NgxBootstrapModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [...components],
  providers: [AuthService]
})

export class CoreModule { }
