import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from './store';
import { SetInitialUser, ResetState } from './store/auth/auth.action';
import { AuthService } from './shared/services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private readonly authService: AuthService,
    private readonly store: Store<AppState>,
  ) { }

  ngOnInit(): void {
    if (this.authService.checkAuthentication()) {
      this.store.dispatch(new SetInitialUser());
    } else {
      this.store.dispatch(new ResetState());
    }
  }
}
